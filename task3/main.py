import json

PRODUCT_VERSION_PATTERN = "Product version:"


def parse_config_file(config_file_path):
    with open(config_file_path, 'r') as config_file:
        data = json.load(config_file)
        return data


def parse_log_file(in_file_path, expected_version, out_file_path, log_level):
    with open(in_file_path, "r") as file:
        version_line = file.readline()
        cur_version = get_version(version_line)

        if cur_version != expected_version:
            raise Exception("Invalid version: {number}".format(number=cur_version))

        result_records = []
        for line in file:
            if line.split(' ')[2] in log_level:
                result_records.append(line)

        if result_records:
            with open(out_file_path, "w") as out_file:
                out_file.writelines(result_records)

            print("Result records saved.")


def get_version(version_str):
    if version_str.startswith(PRODUCT_VERSION_PATTERN):
        version_number = version_str[len(PRODUCT_VERSION_PATTERN):]
        return version_number.strip()
    else:
        raise Exception("Invalid file format, version is missing")


if __name__ == "__main__":
    try:
        data = parse_config_file('config.json')
        print(data["in_file"])
        parse_log_file(in_file_path=data["in_file"],
                       expected_version=data["expected_version"],
                       out_file_path=data["out_file_path"],
                       log_level=data["log_level"])
    except IOError as e:
        print(str(e))
    except Exception as e:
        print(str(e))
