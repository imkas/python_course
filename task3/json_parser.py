import json

with open('config.json', 'r') as file:
    data = json.load(file)

for key, value in data.items():
    print(key, value)
