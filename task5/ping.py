import struct
import socket
import time
import sys

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Incorrect number of parameters")
        print("Usage:")
        print("python ping.py <host> <port_number> <number of attempts>")
        sys.exit(1)

    HOST = sys.argv[1]
    PORT = int(sys.argv[2])
    count = int(sys.argv[3])

    RECEIVE_BUFFER = 1024

    s.connect((HOST, int(PORT)))
    print("Pinging host %s:%s" % (HOST, PORT))

    seq_num = 0

    while seq_num != int(count):
        seq_num += 1
        packet_data = struct.pack("!Hd", seq_num, time.time())
        s.send(packet_data)
        data = s.recv(RECEIVE_BUFFER)

        current_time = time.time()
        (seq, timestamp) = struct.unpack("!Hd", data)
        ping_time = current_time - timestamp
        ping_time *= 1000

        print("seq=%u, time=%.3f ms" % (seq, ping_time))
        time.sleep(1)
    s.close()

