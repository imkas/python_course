import socket
import select
import sys

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Port is not defined")
        print("Usage:")
        print("python server.py <port_number>")
        sys.exit(1)

    if len(sys.argv) > 2:
        print("Too many parameters")
        print("Usage:")
        print("python server.py <port_number>")
        sys.exit(1)

    if len(sys.argv) == 2:
        PORT = int(sys.argv[1])
        CONN_LIST = []
        RECEIVE_BUFFER = 1024

        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server.bind(("0.0.0.0", PORT))
        server.listen()

        CONN_LIST.append(server)

        while True:
            read_sockets, write_sockets, error_sockets = select.select(CONN_LIST, [], [])

            for sock in read_sockets:
                if sock == server:
                    sock_data, address = server.accept()
                    CONN_LIST.append(sock_data)
                else:
                    try:
                        data = sock.recv(RECEIVE_BUFFER)
                        if data:
                            sock.send(data)
                        else:
                            raise ConnectionResetError()

                    except:
                        sock.close()
                        CONN_LIST.remove(sock)
                        continue
