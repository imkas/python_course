@tags @Connect
Feature: Connect
    Scenario: Connect to 'localhost': '1234' with 'c:\temp\out.txt' file
        Given Server is running on 'localhost':'1234'
        And Server Address 'localhost'
        And Server port '1234'
        And Output file 'c:\temp\out.txt'
        When Client Starts
        Then Output file should contain some text

    Scenario: Connect to invalid host
        Given Server is running on 'localhost':'1234'
        And Server Address '0.0.0.0'
        And Server port '1234'
        And Output file 'c:\temp\out.txt'
        When Client Starts
        Then [WinError 10049]

    Scenario: Connect to invalid port
        Given Server is running on 'localhost':'1234'
        And Server Address 'localhost'
        And Server port '1111'
        And Output file 'c:\temp\out.txt'
        When Client Starts
        Then [WinError 10061]

    Scenario: Output file is required
        Given Server is running on 'localhost':'1234'
        When Client Starts without args
        Then Help message is displayed
