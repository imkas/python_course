import socket
import datetime
import sys

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind((sys.argv[1], sys.argv[2]))
sock.listen(1)
conn, addr = sock.accept()

time = datetime.datetime.today().strftime("%Y-%m-%d-%H.%M.%S")
conn.send(time.encode())
conn.close()