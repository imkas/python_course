class Glyph:

    def __init__(self, symbol):
        self.symbol = symbol

    def __eq__(self, other):
        if self.symbol == other.symbol:
            return True

    def display(self):
        raise NotImplementedError

    def replace(self, o, r):
        if self == o:
            self.symbol = r.symbol


class Character(Glyph):

    def __init__(self, symbol):
        self.symbol = symbol

    def display(self):
        print(self.symbol)


class Number(Glyph):

    def __init__(self, symbol):
        self.symbol = int(symbol)

    def display(self):
        print(self.symbol)


class Whitespace(Glyph):

    def __init__(self, symbol=" "):
        self.symbol = symbol

    def display(self):
        print(self.symbol)

    def replace(self, symbol, r):
        pass


def process(stream):
    for glyph in stream:
        glyph.replace(Character('o'), Character('l'))
        glyph.replace(Number(9), Number(5))
        glyph.replace(Whitespace(), Character('k'))  # does nothing
        glyph.display()


def main():
    glyphs = []
    data = input("Enter a string to parse:")
    for char in data:
        if char.isdigit():
            glyphs.append(Number(char))
        elif char.isspace():
            glyphs.append(Whitespace(char))
        elif char.isalpha():
            glyphs.append(Character(char))
    process(glyphs)


if __name__ == '__main__':
    main()
