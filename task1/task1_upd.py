import sys


def create_ordered_task_list(filein, fileout):
    with open(filein, "r") as infile:
        key_value_pair = []
        without_dep = set()
        for line in infile:
            if not line.strip():
                continue
            elif "<-" not in line:
                without_dep.add(line)
            else:
                line1 = line.replace('\n', '')
                key, value = line1.split(" <- ")
                key_value_pair.append((value, key))
        list_del = sort(key_value_pair)
    with open(fileout, "w") as outfile:
        for s in without_dep:
            outfile.write(s + '\n')
        for d in list_del:
            outfile.write(d + '\n')


def sort(p):
    def add_node(g, n):
        if n not in g:
            g[n] = [0]

    def add_arc(g, fn, tn):
        g[fn].append(tn)
        g[tn][0] = g[tn][0] + 1

    task_dict = dict()
    for a, b in p:
        add_node(task_dict, a)
        add_node(task_dict, b)
    for a, b in p:
        add_arc(task_dict, a, b)

    roots = [node for (node, node_info) in task_dict.items() if node_info[0] == 0]
    dependency = []
    while len(roots) != 0:
        changed = len(dependency)

        for r in roots:
            if r not in dependency:
                dependency.append(r)
            else:
                break

        if changed != len(dependency) and len(task_dict) != 0:
            dependency.append("")

        root = roots.pop(0)
        for child in task_dict[root][1:]:
            task_dict[child][0] = task_dict[child][0] - 1
            if task_dict[child][0] == 0:
                roots.append(child)
            # else:
            #     break
        del task_dict[root]
    return dependency


create_ordered_task_list(sys.argv[1], sys.argv[2])

# я использовал допиленный алгоритм Тарьяна для помтроения и обхода графа
# (http://py-algorithm.blogspot.com/2011/09/blog-post.html)
