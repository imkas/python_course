from sqlalchemy import Column, Integer, Boolean, String, DateTime, create_engine, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
import datetime
import sys

' '.join(sys.argv[1:])

engine = create_engine('sqlite:///database.db')

Base = declarative_base(bind=engine)
Session = sessionmaker(bind=engine)


class Employee(Base):
    __tablename__ = 'employees'

    id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(String, nullable=False)
    emp_state = relationship("State", backref="employees")


class Room(Base):
    __tablename__ = 'rooms'

    id = Column(Integer, autoincrement=True, primary_key=True)
    room_number = Column(Integer, autoincrement=False, nullable=False)
    room_description = Column(String, nullable=False, default='New room')
    # __table_args__ = (CheckConstraint(room_number > 0, name='room_number_positive'), {})
    room_state = relationship("State", backref="rooms")


class State(Base):
    __tablename__ = 'state'

    id = Column(Integer, autoincrement=True, primary_key=True)
    employee_id = Column(Integer, ForeignKey('employees.id'))
    room_id = Column(Integer, ForeignKey('rooms.id'))
    date = Column(DateTime, default=datetime.datetime.now())
    entrance = Column(Boolean, nullable=False, default=True)
    # employee = relationship("Employee", foreign_keys=[employee_id])
    # room = relationship("Room", foreign_keys=[room_id])


Base.metadata.create_all(engine)


def add_employee(employee_name):
    session = Session()
    new_employee = Employee(name=employee_name)
    session.add(new_employee)
    session.commit()


def add_room(room_nmb, room_dscr):
    session = Session()
    new_room = Room(room_number=room_nmb, room_description=room_dscr)
    session.add(new_room)
    session.commit()


def add_transition(empname, roomnmb, date):
    session = Session()
    ent_date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
    employee = session.query(Employee).filter(Employee.name == empname).first()
    room = session.query(Room).filter(Room.room_number == roomnmb).first()
    state = session.query(State).filter(State.employee_id == employee.id).order_by(State.id.desc()).first()

    if not state:
        session.add(State(employee_id=employee.id, room_id=room.id, date=ent_date, entrance=True))
    else:
        session.add(State(employee_id=employee.id, room_id=state.room_id, date=ent_date, entrance=False))
        session.add(State(employee_id=employee.id, room_id=room.id, date=ent_date, entrance=True))
    session.commit()


def rooms(empname, day):
    session = Session()

    start_date = datetime.datetime.strptime(day, '%Y-%m-%d')
    end_date = datetime.datetime.combine(start_date, datetime.time(23, 59, 59))

    emp_id = session.query(Employee).filter(Employee.name == empname).first()
    rooms_list = session.query(Room).filter(Room.id == State.room_id). \
        filter(State.employee_id == emp_id.id). \
        filter(State.date.between(start_date, end_date)).order_by(State.id).all()
    for room in rooms_list:
        print(str(room.room_number) + ' ' + room.room_description)
    session.commit()


def employees(room_nmb, day):
    session = Session()

    start_date = datetime.datetime.strptime(day, '%Y-%m-%d')
    end_date = datetime.datetime.combine(start_date, datetime.time(23, 59, 59))

    rm_id = session.query(Room).filter(Room.room_number == room_nmb).first()
    employee_list = session.query(Employee).filter(Employee.id == State.employee_id). \
        filter(State.room_id == rm_id.id). \
        filter(State.date.between(start_date, end_date)).order_by(State.id).all()
    for emp in employee_list:
        print(emp.name)
    session.commit()


def employee_in_room(empname, room_nmb):
    session = Session()

    rm_id = session.query(Room).filter(Room.room_number == room_nmb).first()
    emp_id = session.query(Employee).filter(Employee.name == empname).first()

    state = session.query(State).filter(State.employee_id == Employee.id). \
        filter(State.room_id == Room.id). \
        filter(State.room_id == rm_id.id). \
        filter(State.employee_id == emp_id.id). \
        order_by(State.id.desc()).first()

    if state.entrance == True:
        print(empname + ' is in room number ' + str(room_nmb))
    else:
        print(empname + ' is out of room number ' + str(room_nmb))


# add_employee('John')
# add_employee('Josh')
# add_employee('Ivan')
#
# add_room(101, 'Assembly Hall')
# add_room(102, 'CEO Room')
# add_room(103, 'Dev')
#
# add_transition('Ivan', 103)


# add_transition('John', '103', datetime.datetime(2018, 4, 15, 18, 00, 00))
#
# rooms('Ivan', '2018-04-15')
# employees(103, '2018-04-15')

# employee_in_room('Ivan', 101)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print('No command was found.')
        print('Usage:')
        print('python task4.py add_employee <employee name>')
        print('python task4.py add_room <room number> <room description>')
        print('python task4.py add_transition <employee name> <room number> <date>')
        print('date should be in Y-m-d HH-mm-ss format, f.e. 2018-04-14 17:17:17')
        print('python task4.py room <employee name> <date>')
        print('date should be in Y-m-d format, f.e. 2018-04-14')
        print('python task4.py employee <room number> <date>')
        print('date should be in Y-m-d format, f.e. 2018-04-14')
        print('python task4.py employee_in_room <employee name> <room number>')
        sys.exit(1)

    if sys.argv[1] == 'add_employee':
        add_employee(str(sys.argv[2]))
    elif sys.argv[1] == 'add_room':
        add_room(int(sys.argv[2]), str(sys.argv[3]))
    elif sys.argv[1] == 'add_transition':
        add_transition(str(sys.argv[2]), int(sys.argv[3]), str(sys.argv[4]))
    elif sys.argv[1] == 'room':
        rooms(str(sys.argv[2]), str(sys.argv[3]))
    elif sys.argv[1] == 'employee':
        employees(int(sys.argv[2]), str(sys.argv[3]))
    elif sys.argv[1] == 'employee_in_room':
        employee_in_room(str(sys.argv[2]), int(sys.argv[3]))
    else:
        print('Unknown command.')
        print('Usage:')
        print('python task4.py add_employee <employee name>')
        print('python task4.py add_room <room number> <room description>')
        print('python task4.py add_transition <employee name> <room number> <date>')
        print('date should be in Y-m-d HH-mm-ss format, f.e. 2018-04-14 17:17:17')
        print('python task4.py room <employee name> <date>')
        print('date should be in Y-m-d format, f.e. 2018-04-14')
        print('python task4.py employee <room number> <date>')
        print('date should be in Y-m-d format, f.e. 2018-04-14')
        print('python task4.py employee_in_room <employee name> <room number>')
