from pywinauto import Application
import time

app = Application().Start("notepad.exe")
notepad = app.Notepad
notepad.Wait('ready')

notepad.Edit.set_text("1 2 3 4 5 6 7 8 9 0 blah-blah-blah")

notepad.menu_select("Format->Font...")
app.Font.FontComboBox.select("Comic Sans MS")
app.Font.FontStyleComboBox.select("Bold Italic")
app.Font.SizeEditBox.set_text("17")
app.Font.OK.Click()

notepad.menu_select("File->Page Setup...")
app.PageSetup.SizeComboBox.select("A3")
app.PageSetup.Landscape.Click()
app.PageSetup.OK.Click()

notepad.menu_select("File->Print..")
app.Print.Print.Click()
time.sleep(1)
printDlg = Application().connect(title_re=".*Bullzip PDF Printer*")
printDlg.ThunderRT6FormDC.Wait('ready')
printDlg.ThunderRT6FormDC[u'...'].Click()
printDlg.SaveAsDlg.Wait('ready')
printDlg.SaveAsDlg.Edit.set_text("task6.pdf")
printDlg.SaveAsDlg.Save.Click()
printDlg.ThunderRT6FormDC.Save.Click()

app.kill()
